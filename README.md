# Venturus Bikers - Client

## Dependencies

This project was generated and uses Angular CLI to run, so make sure you have it installed properly with `npm install -g @angular/cli`

## Preparing the workspace

After cloning this repository, navigate to its root directory and simply run the command `npm install` to install all of its dependencies.

## Server

Make sure that the server and MongoDB instance are set up and running before starting the app

## Running the app

At the root directory run the command `ng serve --open` to start the client. This command will automaticaly open the application at the browser for you.

### Additional info

Used versions:  
Node: v8.11.2  
NPM: 5.5.1