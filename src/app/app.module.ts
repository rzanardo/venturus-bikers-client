import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { BikersFormComponent } from './components/bikers-form/bikers-form.component';
import { BikersListComponent } from './components/bikers-list/bikers-list.component';
import { BikersPageComponent } from './components/bikers-page/bikers-page.component';
import { FakeBreadcrumbComponent } from './components/fake-breadcrumb/fake-breadcrumb.component';
import { FakeNavigationComponent } from './components/fake-navigation/fake-navigation.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { HelpComponent } from './components/help/help.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { BikersProvider } from './providers/bikers-provider';
import { RadiobuttonComponent } from './components/radiobutton/radiobutton.component';

@NgModule({
  declarations: [
    AppComponent,
    BikersPageComponent,
    BikersFormComponent,
    BikersListComponent,
    HeaderComponent,
    FooterComponent,
    CheckboxComponent,
    HelpComponent,
    FakeBreadcrumbComponent,
    FakeNavigationComponent,
    NotFoundComponent,
    RadiobuttonComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    FontAwesomeModule,
    AppRoutingModule
  ],
  providers: [
    BikersProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor() {
  }
}
