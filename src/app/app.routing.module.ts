import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HelpComponent } from "./components/help/help.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { BikersPageComponent } from "./components/bikers-page/bikers-page.component";

const routes: Routes = [
    { path: '', component: BikersPageComponent },
    { path: 'help', component: HelpComponent },
    { path: 'bikers', component: BikersPageComponent },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {

}