import { Component, EventEmitter, Input, Output } from "@angular/core";
import { faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';
import { Biker } from "../../models/biker";

@Component({
    selector:'bikers-list',
    templateUrl:'bikers-list.component.html',
    styleUrls: ['bikers-list.component.css', '../../app.component.css']
})
export class BikersListComponent {

    private faTrash;
    private faPencilAlt;

    constructor() {
        this.faTrash = faTrash;
        this.faPencilAlt = faPencilAlt;
    }

    @Input() bikers: Biker[];
    @Output() messageEvent = new EventEmitter<Biker>();
    @Output() deleteMessageEvent = new EventEmitter<Biker>();

    loadBiker(biker: Biker) {
        this.messageEvent.emit(biker);
    }

    deleteBiker(biker: Biker) {
        if (confirm(`Do you really want to delete the biker: ${biker.fullName}?`)) {
            this.deleteMessageEvent.emit(biker);
        } 
    }

    displayDaysOfTheWeek(biker: Biker): string {
        if (this.isAllWeekendDaysSelected(biker) && !this.isAnyWorkingDaySelected(biker)) {
            return "Weekends"
        } else if (this.isAllWorkingDaysSelected(biker) && !this.isAnyWeekendSelected(biker)) {
            return "Week days"
        } else if (this.isAllWeekendDaysSelected(biker) && this.isAllWorkingDaysSelected(biker)) {
            return "Every day"
        } else {
            return this.getDaysOfTheWeekString(biker)
        }
    }

    private getDaysOfTheWeekString(biker: Biker): string {
        let selectedDays = new Array();

        if (biker.dowMonday) {
            selectedDays.push("Mon");
        }

        if (biker.dowTuesday) {
            selectedDays.push("Tue");
        }

        if (biker.dowWednesday) {
            selectedDays.push("Wed");
        }

        if (biker.dowThursday) {
            selectedDays.push("Thu");
        }

        if (biker.dowFriday) {
            selectedDays.push("Fri");
        }

        if (biker.dowSaturday) {
            selectedDays.push("Sat");
        }

        if (biker.dowSunday) {
            selectedDays.push("Sun");
        }

        return selectedDays.toString()
    }

    private isAllWorkingDaysSelected(biker: Biker): Boolean {
        return biker.dowMonday && biker.dowTuesday && biker.dowWednesday && biker.dowThursday && biker.dowFriday;
    }

    private isAnyWorkingDaySelected(biker: Biker): Boolean {
        return biker.dowMonday || biker.dowTuesday || biker.dowWednesday || biker.dowThursday || biker.dowFriday;
    }

    private isAllWeekendDaysSelected(biker: Biker): Boolean {
        return biker.dowSaturday && biker.dowSunday;
    }

    private isAnyWeekendSelected(biker: Biker): Boolean {
        return biker.dowSaturday || biker.dowSunday;
    }
}