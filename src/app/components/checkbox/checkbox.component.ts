import { Component, OnInit, Input } from '@angular/core';
import { Biker } from 'src/app/models/biker';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {

  @Input() biker: Biker;
  constructor() { }

  ngOnInit() {
  }

}
