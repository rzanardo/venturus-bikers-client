import { Component, OnInit } from '@angular/core';
import { faBicycle } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css', '../../app.component.css']
})
export class HelpComponent implements OnInit {

  private faBicycle;

  constructor() { 
    this.faBicycle = faBicycle;
  }

  ngOnInit() {
  }

}
