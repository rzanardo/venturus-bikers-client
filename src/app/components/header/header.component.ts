import { Component, OnInit } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons'
import { faUser } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css', '../../app.component.css']
})
export class HeaderComponent implements OnInit {

  private faBars;
  private faUser;

  constructor() { 
    this.faBars = faBars;
    this.faUser = faUser;
  }

  ngOnInit() {
  }

  logout() {
    alert("I'm sorry Dave. I'm afraid I can't do that.")
  }
}
