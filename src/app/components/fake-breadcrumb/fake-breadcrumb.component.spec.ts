import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FakeBreadcrumbComponent } from './fake-breadcrumb.component';

describe('FakeBreadcrumbComponent', () => {
  let component: FakeBreadcrumbComponent;
  let fixture: ComponentFixture<FakeBreadcrumbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FakeBreadcrumbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FakeBreadcrumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
