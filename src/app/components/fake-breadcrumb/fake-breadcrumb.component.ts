import { Component, OnInit } from '@angular/core';
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { faLifeRing } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'fake-breadcrumb',
  templateUrl: './fake-breadcrumb.component.html',
  styleUrls: ['./fake-breadcrumb.component.css', '../../app.component.css']
})
export class FakeBreadcrumbComponent implements OnInit {

  private faHome;
  private faLifeRing;

  constructor() { 
    this.faHome = faHome;
    this.faLifeRing = faLifeRing;
  }

  ngOnInit() {
  }

}
