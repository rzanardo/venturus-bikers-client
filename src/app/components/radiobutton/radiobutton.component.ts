import { Component, OnInit, Input } from '@angular/core';
import { Biker } from 'src/app/models/biker';

@Component({
  selector: 'app-radiobutton',
  templateUrl: './radiobutton.component.html',
  styleUrls: ['./radiobutton.component.css']
})
export class RadiobuttonComponent implements OnInit {

  @Input() biker: Biker;
  
  constructor() { }

  ngOnInit() {
  }

}
