import { Component } from "@angular/core";
import { Biker } from "../../models/biker";
import { BikersProvider } from "../../providers/bikers-provider";

@Component({
    selector: 'bikers-page',
    templateUrl: 'bikers-page.component.html',
    styleUrls: ['bikers-page.component.css']
})
export class BikersPageComponent {

    /*
     * both these properties are bound to the LIST and FORM components
     * this component is a wrapper of everything that manipulates real data
     * and works as a kind of gateway of the objects to the other components
     */
    private bikers: Biker[];
    private biker: Biker = new Biker();

    constructor(private _bikersProvider: BikersProvider) {
        this.retrieveBikersList()
    }

    /*
     * this method is responsible for efectively requesting to the database the available documents
     */
    retrieveBikersList() {
        this._bikersProvider.getAllBikers().subscribe(
            bikers => {
                this.bikers = bikers;
            },
            error => {
                console.log(error);
            }
        )
    }

    /*
     * this method adds the biker object received from the list to the form, to be available for editing
     */
    loadBiker($event) {
        this.biker = $event
    }

    /*
     * this method is responsible for efectively requesting to the database to insert a new biker
     */
    saveBiker($event) {
        this._bikersProvider.saveBiker($event).subscribe(
            biker => {
                console.log(`Biker saved: ${biker}`)
                this.retrieveBikersList();
                this.biker = new Biker();
            },
            error => {
                console.log(error);
            }
        )
    }

    /*
     * this method is responsible for efectively requesting to the database to delete the chosen biker
     */
    deleteBiker($event) {
        this._bikersProvider.deleteBiker($event).subscribe(
            () => {
                this.retrieveBikersList();
            },
            error => {
                console.log(error);
            }
        )
    }
}