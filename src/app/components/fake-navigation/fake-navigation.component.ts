import { Component, OnInit } from '@angular/core';
import { faPuzzlePiece } from '@fortawesome/free-solid-svg-icons'
import { faFutbol } from '@fortawesome/free-solid-svg-icons'
import { faBicycle } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'fake-navigation',
  templateUrl: './fake-navigation.component.html',
  styleUrls: ['./fake-navigation.component.css', '../../app.component.css']
})
export class FakeNavigationComponent implements OnInit {

  private faPuzzlePiece;
  private faFutbol;
  private faBicycle;

  constructor() { 
    this.faPuzzlePiece = faPuzzlePiece;
    this.faFutbol = faFutbol;
    this.faBicycle = faBicycle;
  }

  ngOnInit() {
  }

}
