import { Component, Input, Output, EventEmitter } from "@angular/core";
import { Biker } from "../../models/biker";

@Component({
    selector: 'bikers-form',
    templateUrl: 'bikers-form.component.html',
    styleUrls: ['bikers-form.component.css', '../../app.component.css']
})
export class BikersFormComponent {

    @Input() biker: Biker;
    @Output() messageEvent = new EventEmitter<Biker>();

    /*
     * this method is responsible for emiting the event to the BikersPageComponent
     * passing the biker as parameter to be saved at the database
     */
    saveBiker() {
        let invalidField = this.validateFields();

        if (invalidField == null) {
            this.messageEvent.emit(this.biker);
            return
        } else {
            alert(`The field ${invalidField} must be filled.`);
        }
    }

    /*
     * this method validates both if the field is null or if is empty, to prevent clicking to
     * EDIT an already existing register and erasing its value
     */
    validateFields(): string {
        if (this.biker.fullName == null || this.biker.fullName == "") {
            return "full name";
        }

        if (this.biker.email == null || this.biker.email == "") {
            return "e-mail";
        }

        if (this.biker.groupRide == null) {
            return "'Ride in group?'";
        }

        if (!this.biker.dowMonday && !this.biker.dowTuesday && !this.biker.dowWednesday && !this.biker.dowThursday && !this.biker.dowFriday && !this.biker.dowSaturday && !this.biker.dowSunday) {
            return "'Days of the week'"
        }
        return null;
    }

    /*
     * this method clears the "current" biker cleaning the form fields with it
     */
    cancel() {
        this.biker = new Biker();
    }
}