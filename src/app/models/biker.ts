import { Model } from "./model";

/*
    these fields that represents the checkboxes weren't even by far the best solution
    but i didn't have time to implement something better, so, for now it'll work
*/
export class Biker extends Model {
    _id: number;
    fullName: string;
    email: string;
    city: string;
    groupRide: string;
    dowMonday: boolean;
    dowTuesday: boolean;
    dowWednesday: boolean;
    dowThursday: boolean;
    dowFriday: boolean;
    dowSaturday: boolean;
    dowSunday: boolean;
    registrationDate: Date;
}