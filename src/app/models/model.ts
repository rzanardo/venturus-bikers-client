//extensible class used to create object models
export class Model {
    constructor(object?) {
        Object.assign(this, object);
    }
  }