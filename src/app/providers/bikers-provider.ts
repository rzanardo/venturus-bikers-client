import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Biker } from "../models/biker";

@Injectable()
export class BikersProvider {

    private baseUrl = 'http://localhost:3000';
    private headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');

    constructor(private _httpClient: HttpClient) { }

    /*
     * this method returns a list (an array of Biker object) of all the bikers present at the database
     */
    getAllBikers() {
        let url = this.baseUrl + "/biker"
        return this._httpClient.get<Biker[]>(url)
    }

    /*
     * this method either updates or saves the bikers at the database. it figures out which method to call by checking if the biker already has an ID or not
     */
    saveBiker(biker: Biker) {
        if (biker._id != null) {
            let url = this.baseUrl + "/biker/" + biker._id;
            return this._httpClient.put(url, biker, { headers: this.headers });
        } else {
            let url = this.baseUrl + "/biker";
            return this._httpClient.post<Biker>(url, biker, { headers: this.headers });
        }
    }

    /*
     * this method deletes a biker from the database
     */
    deleteBiker(biker: Biker) {
        let url = this.baseUrl + "/biker/" + biker._id;
        return this._httpClient.delete(url, { headers: this.headers });
    }
}